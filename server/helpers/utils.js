deleteStudentFromTable = (req, res, client) => {
    var id = req.body.id;
    client.query(`DELETE FROM students WHERE user_id = ${id}`, [], function (err, result) {
        if (err) {
            res.status(401).send("error")
        } else {
            res.status(200).send("ok")
        }
    });
};

autorizationValidationCheck = (req, res, client) =>{
    var user = {
        login: req.body.login,
        password: req.body.password,
    };
    var baseLogin;
    var basePassword;
    client.query(`SELECT * FROM teachers WHERE login = '${user.login}';`, [], function (err, result) {
        for (var key in result.rows) {

            baseLogin = result.rows[key].login;
            basePassword = result.rows[key].password;
            teacherId = result.rows[key].teachers_id;

        }
        if (baseLogin === `${user.login}` && basePassword === `${user.password}`) {
            res.json(result.rows)
        } else {
            authorizated = "";
            res.status(401).send('Unauthorized ');

        }
    });
};

registrationRequest = (req, res, client) => {
    var user = {
        login: req.body.login,
        password: req.body.password,
        email: req.body.email,
        phone: req.body.phone,
        keyword: req.body.keyword
    };
    client.query(`SELECT * FROM teachers WHERE login = '${user.login}';`, [], function (err, result) {

        var baselogin;
        for (var key in result.rows) {

            baselogin = result.rows[key].login;
        }
        if (baselogin !== `${user.login}`) {
            var newUser = `INSERT INTO teachers(login, password, email, phone_number,keyword) VALUES ('${user.login}', '${user.password}', '${user.email}', '${user.phone}','${user.keyword}')`;
            client.query(newUser, []);
            res.status(200).send('Ok');
        } else {
            res.status(400).send('Bad Request ');
        }
    });
};

accountSettingsRequest = (req, res, client) =>{
    client.query(`SELECT * FROM teachers WHERE teachers_id = '${teacherId}';`, [], function (err, result) {
        if(result.rows !== undefined) {
            res.json(result.rows);
        }
    });
};

createGroupRequest = (req, res, client) => {
    var newGroup = `INSERT INTO groups(groupname, teacher_id) VALUES 
    ('${req.body.groupName}', ${req.body.teachers_id})`;
    client.query(newGroup, [],
        function (err, result) {
            if (err) {
                res.status(400).send(err);
            }
        });
    client.query(`SELECT * FROM groups WHERE groupname = '${req.body.groupName}';`, [], function (err, result) {
        if (err) {
            res.status(401).send("error")
        } else {
            res.json(result.rows[0].groups_id);
        }
    });
};

createStudent = (req, res, client) =>{
    var user = {
        username: req.body.username,
        age: req.body.age,
        lastname: req.body.lastname,
        city: req.body.city,
        groups_id: req.body.groups_id
    };

    var newUser = `INSERT INTO students( firstname, lastname, age, city, groups_id) VALUES
    ('${user.username}', '${user.lastname}', '${user.age}', '${user.city}', ${user.groups_id}) RETURNING user_id`;
    client.query(newUser, [],
        function (err, result) {
            if (err) {
                res.status(400).send("error")
            } else {
                res.json(result.rows)
            }
        })
};

updateStudent = (req, res, client) => {
    var userID = {
        id: req.body.id
    };
    var queryColomn = [
        "firstname",
        "lastname",
        "age",
        "city"
    ];

    var queryComand = "";

    var valueCounter = 0;
    var counterLink = 1;

    var user = {
        username: req.body.username,
        lastname: req.body.lastname,
        age: req.body.age,
        city: req.body.city
    };

    var upgradeSQL = [];

    Object.keys(user).forEach(function (key) {
        if (!(this[key].length === 0)) {
            upgradeSQL.push(`${this[key]}`);
            // if ()
            queryComand += queryColomn[valueCounter] + "= $" + counterLink + ",";

            counterLink++
        }
        valueCounter++;
    }, user);

    queryComand = queryComand.substring(0, queryComand.length - 1);

    client.query(`UPDATE students SET ${queryComand} WHERE user_id = ${userID.id}`,
        upgradeSQL,
        function (err, result) {
            if (err) {
                res.status(401).send("error")
            } else {
                res.status(200).send("ok")
            }

        });
};

accountUpdate = (req, res, client) => {
    var teacherId = {
        id: req.body.teachers_id
    };
    var teachersSqlColumn = [
        "login",
        "password",
        "email",
        "phone_number",
        "about_myself",
        // "teacher_icon"
    ];

    var queryComand = "";

    var valueCounter = 0;
    var counterLink = 1;

    var user = {
        login: req.body.login,
        password: req.body.password,
        email: req.body.email,
        phone_number: req.body.phone,
        about_myself: req.body.aboutMyself,
        // teacher_icon: req.body.teacher_icon
    };

    var upgradeSQL = [];

    Object.keys(user).forEach(function (key) {
        if (!(this[key].length === 0)) {
            upgradeSQL.push(`${this[key]}`);
            // if ()
            queryComand += teachersSqlColumn[valueCounter] + "= $" + counterLink + ",";

            counterLink++
        }
        valueCounter++;
    }, user);

    queryComand = queryComand.substring(0, queryComand.length - 1);
    console.log(teacherId.id, upgradeSQL, queryComand);

    client.query(`UPDATE teachers SET ${queryComand} WHERE teachers_id = ${teacherId.id}`,
        upgradeSQL,
        function (err, result) {
            if (err) {
                console.log(err);
            }
            console.log(result);
        });
};

forgottenPass = (req, res, client) => {
    var user = {
        login: req.body.login,
        keyword: req.body.keyword,

    };

    client.query(`SELECT * FROM teachers WHERE login = '${user.login}';`, [], function (err, result) {

        var baseLogin;
        var basekeyword;
        var  basePassword ;
        for (var key in result.rows) {

            baseLogin = result.rows[key].login;
            basekeyword = result.rows[key].keyword;
            basePassword = result.rows[key].password;

        }
        console.log(basePassword, basekeyword, baseLogin);
        if (baseLogin === `${user.login}` && basekeyword === `${user.keyword}`) {
            authorizated = req.body.login;
            res.json(basePassword);
        } else {
            authorizated = "";
            res.status(401).send('Unauthorized ');
        }
    });
};

deleteGroup = (req, res, client) => {
    var id = req.body.groupId;

    client.query(`DELETE FROM students WHERE groups_id = ${id}`, [], function (err, result) {
        if (err) {
            console.log(err)
        }
    });
    client.query(`DELETE FROM groups WHERE groups_id = ${id}`, [], function (err, result) {
        if (err) {
            res.status(401).send("error")
        } else {
            res.status(200).send("ok")
        }
    });
};

clearStudents = (req, res, client) => {
    var id = req.body.groupId;

    client.query(`DELETE FROM students WHERE groups_id = ${id}`, [], function (err, result) {
        if (err) {
            res.status(401).send("error")
        } else {
            res.status(200).send("ok")
        }
    });
};

updateGroup = (req, res, client) => {
    client.query(`UPDATE groups SET groupname = '${req.body.name}' WHERE groups_id = ${req.body.id};`, [], (err, response) => {
        if (err) {
            res.status(502).send("SERVER ERROR");
        } else {
            res.status(200).send("ok");
        }
    });
};

sendImage = (req, res, client) => {
    client.query(`UPDATE teachers SET teacher_icon = '${req.body.img}' WHERE teachers_id = ${teacherId};`, [], (err, response) => {
        if (err) {
            res.status(502).send("SERVER ERROR");
        } else {
            res.json(req.body.img);
        }
    });
};

resetSettings = (req, res, client) => {
    client.query(`DELETE FROM students WHERE user_id >= 1`, [], function (err, result) {
        if (err) {
            console.log("error")
        }
    });
    client.query(`DELETE FROM groups WHERE groups_id >=1`, [], function (err, result) {
        if (err) {
            res.status(401).send("error")
        } else {
            res.status(200).send("ok")
        }
    });
};

getGroupOfStudents = (req, res, client) => {
    var userId = +req.body.name;
    client.query(`SELECT * FROM students WHERE groups_id = ${userId} ORDER BY user_id;`, [], function (err, result) {
        res.json(result.rows);
    });
};

getAllGroups = (req, res, client) => {

    client.query(`SELECT * FROM groups WHERE teacher_id = ${req.body.teachers_id};`, [], function (err, result) {

        res.json(result.rows);
    });
}

module.exports ={
    deleteStudentFromTable,
    autorizationValidationCheck,
    registrationRequest,
    accountSettingsRequest,
    createGroupRequest,
    createStudent,
    updateStudent,
    accountUpdate,
    forgottenPass,
    deleteGroup,
    clearStudents,
    updateGroup,
    sendImage,
    resetSettings,
    getGroupOfStudents,
    getAllGroups
};