let express = require("express");
let app = express();
let path = require("path");
let fs = require('fs');
let bodyParser = require("body-parser");
let constants = require("./helpers/constants");
let utils = require("./helpers/utils");
let authorizated;
let teacherId;

app.use(express.static(path.join(__dirname, 'static')));
app.use(function (req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
});
app.use(bodyParser.json({limit: '10mb', extended: true}));
const {Client} = require('pg');
const client = new Client({
    user: 'postgres',
    host: 'localhost',
    database: 'postgres',
    password: 'postgres',
    port: 5432,
});

client.connect(function (err) {
    console.log("Connected!");
});

app.post(constants.deleteStudentUrl,  (req, res) => utils.deleteStudentFromTable(req, res, client));
app.post(constants.authorizationUrl,  (req, res) => utils.autorizationValidationCheck(req, res, client));
app.post(constants.registrationUrl,  (req, res) => utils.registrationRequest(req, res, client));
app.post(constants.groupStudentUrl, (req, res) => utils.getGroupOfStudents(req, res, client));
app.post(constants.getAllGroupsUrl, (req, res) => utils.getAllGroups(req, res, client));
app.get(constants.accountSettingUrl,  (req, res) => utils.accountSettingsRequest(req, res, client));
app.post(constants.groupsUrl,  (req, res) => utils.createGroupRequest(req, res, client));
app.post(constants.insertStudentUrl, (req, res) => utils.createStudent(req, res, client));
app.post(constants.updateStudentUrl, (req, res) => utils.updateStudent(req, res, client));
app.post(constants.accountupdateUrl,  (req, res) => utils.accountUpdate(req, res, client));
app.post(constants.forgottenPassUrl,  (req, res) => utils.forgottenPass(req, res, client));
app.post(constants.deleteGroupUrl,  (req, res) => utils.deleteGroup(req, res, client));
app.post(constants.clearStudentsUrl,  (req, res) => utils.clearStudents(req, res, client) );
app.post(constants.updateGroupUrl,  (req, res) => utils.updateGroup(req, res, client));
app.post(constants.sendImageUrl,  (req, res) => utils.sendImage(req, res, client) );
app.post(constants.resetSettingsUrl,  (req, res) => utils.resetSettings(req, res, client));

app.listen(constants.port, function () {
    console.log("port: " + constants.port)
});