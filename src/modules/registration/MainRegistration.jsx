import React from "react";
import "../../styles/registration.less"
//
// , text, callback

const Registration = props => {
    const {classname, callback} = props;
    return(
        <main className="wrapper">
            <div className="content-box">
                <h1 className="registration__title" id="regLabel">Registration</h1>
                <div className="registration-form" action="">
                    <div className="registration-form__item">
                        <label htmlFor="login" id="loginLabel">Login*</label>
                        <div className="alert-error" id="errorLogin"></div>
                        <input type="text" placeholder="log" id="login" required />
                    </div>
                    <div className="registration-form__item">
                        <label htmlFor="password1" id="passLabel">Password*</label>
                        <div className="alert-error" id="errorPassword1"></div>
                        <input type="password" placeholder="pass" id="password1" required />
                    </div>
                    <div className="registration-form__item">
                        <label htmlFor="password2" id="pass2Label">Confirm password*</label>
                        <div className="alert-error" id="errorPassword2"></div>
                        <input type="password" placeholder="pass2" id="password2" required />
                    </div>
                    <div className="registration-form__item">
                        <label htmlFor="email" id="emailLabel">E-mail*</label>
                        <div className="alert-error" id="errorEmail"></div>
                        <input type="email" placeholder="email" id="email" required />
                    </div>
                    <div className="registration-form__item">
                        <label htmlFor="phone" id="phoneLabel">Phone number*</label>
                        <div className="alert-error" id="errorPhone"></div>
                        <input type="tel" placeholder="phone" id="phone" required />
                    </div>
                    <div className="registration-form__item">
                        <label htmlFor="keyword" id="keywordLabel">Keyword</label>
                        <input type="text" placeholder="keyword" id="keyword" required />
                            <label htmlFor="keyword" className="notification" id="keywordNotification">The keyword needed for password recovery
                            </label>
                    </div>
                    <div className="registration-form__item">
                        <div div className="registration-form__message" id="message"></div>
                    </div>
                    <div className="buttons">
                        <div className="dic">
                            <button id="registrationBtn" className="button">Sign up</button>
                        </div>
                        <div className="dic">
                            <a className="button" id="close" onClick={callback}>Return to login</a>
                        </div>
                    </div>
                </div>
            </div>

        </main>
    );
};
export default Registration