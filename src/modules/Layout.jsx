import React, { Component} from "react";

// import "../styles/autorization.less"
import Header from "./header/Header"
import Main from "./authorization/MainAuthorization"
import Registration from "./registration/MainRegistration";
class Layout extends React.Component{
     state = {
        buttonValue:false
    };

     toggleMain = () =>{this.setState(prevState => ({
         buttonValue: !prevState.buttonValue
     }))
     };

    render(){
        return(
            <div className="main-wrapper">
                <Header className = {"header"} />
                {this.state.buttonValue === false ? <Main className = {"wrapper"} callback ={this.toggleMain}/> :
                    <Registration callback ={this.toggleMain}/>}
            </div>
        );
    }
}
export default Layout;

