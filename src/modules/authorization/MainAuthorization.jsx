import React from "react";


// , text, callback

const Main = props => {
    const {classname, callback} = props;
    return(
<main className="wrapper">
    <div className="main">
        <div className="main__authorization-title">
            <h1 className="authorization_title" id="textAuthorization">Authorization</h1>
        </div>
        <div className="main__authorization-form">
            <div className="authorization-form-item">
                <div className="authorization-form__label">
                    <label htmlFor="loginAvt" id="labelLogin">Login:</label>
                </div>

                <div className="authorization-form__input-wrapper">
                    <input className="custom-input" type="text" id="loginAvt" placeholder="write your login "
                           required />
                </div>
            </div>
            <div className="authorization-form-item">
                <div className="authorization-form__label">
                    <label htmlFor="passwordAvt" id="labelPassword">Password:</label>
                </div>
                <div className="authorization-form__input-wrapper">
                    <input className="custom-input" type="password" id="passwordAvt"
                           placeholder="write your password " required/>
                </div>
            </div>
            <div className="authorization-form-item">
                <div className="authorization-form-item__message" id="message"></div>
            </div>
            <div className="authorization-form-item">
                <button className="custom-button authorization-form__btn" id="logInBtn">Log In</button>
            </div>
            <div className="authorization-form-item">
                <button className="custom-button authorization-form__btn" id="registrationBtnAvt" onClick={callback}>Registration
                </button>
            </div>
            <div className="authorization-form-item">
                <button className="custom-button authorization-form__btn" id="forgottenPassword">Forgotten
                    Password
                </button>
            </div>
            </div>
        </div>
    </main>
    );
};
export default Main